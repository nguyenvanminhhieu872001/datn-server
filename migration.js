import db from "./config/database.js";
import dotenv from "dotenv";

dotenv.config();

(async () => {
  await db.sync();
})();
