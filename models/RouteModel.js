import { Sequelize } from "sequelize";
import db from "../config/database.js";
import slugify from "slugify";
const { DataTypes } = Sequelize;

const Route = db.define(
  "Route",
  {
    startPoint: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
      indexes: [{ fields: ["startPoint"] }],
    },
    endPoint: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
      indexes: [{ fields: ["endPoint"] }],
    },
    frequency: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
        min: 0,
      },
    },
    busTotal: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
        min: 0,
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "inactive",
      validate: {
        notEmpty: true,
        isIn: [["inactive", "active", "repair", "maintenance", "disable"]],
      },
    },
    slug: {
      type: DataTypes.STRING,
      unique: false,
      indexes: [{ fields: ["slug"] }],
    },
  },
  {
    freezeTableName: true,
    hooks: {
      beforeValidate: (route, options) => {
        // Tạo giá trị slug từ giá trị startPoint và endPoint trước khi validate
        route.slug = slugify(`${route.startPoint}-${route.endPoint}`, {
          lower: true,
        });
      },
    },
    afterSync: async () => {
      // Áp dụng chỉ mục Full-Text Search cho cột startPoint, endPoint, và slug
      await db.query(`
          ALTER TABLE Route ADD FULLTEXT INDEX idx_startPoint_full_text_search (startPoint);
          ALTER TABLE Route ADD FULLTEXT INDEX idx_endPoint_full_text_search (endPoint);
          ALTER TABLE Route ADD FULLTEXT INDEX idx_slug_full_text_search (slug);
        `);
    },
  }
);

export default Route;
