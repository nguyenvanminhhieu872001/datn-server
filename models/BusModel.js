// BusModel.js
import { Sequelize } from "sequelize";
import db from "../config/database.js";
import slugify from "slugify";
const { DataTypes } = Sequelize;

const Bus = db.define(
  "Bus",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
    },
    seat: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "normal",
      validate: {
        notEmpty: true,
        isIn: [["normal", "sleeper"]],
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "inactive",
      validate: {
        notEmpty: true,
        isIn: [["inactive", "active", "repair", "maintenance", "disable"]],
      },
    },
    slug: {
      type: DataTypes.STRING,
      unique: false,
      indexes: [{ fields: ["slug"] }],
    },
  },
  {
    freezeTableName: true,
    hooks: {
      beforeValidate: (bus, options) => {
        // Tạo giá trị slug từ giá trị name trước khi validate
        bus.slug = slugify(bus.name, { lower: true });
      },
    },
    afterSync: async () => {
      // Áp dụng chỉ mục Full-Text Search cho cột slug
      await db.query(`
          ALTER TABLE Bus ADD FULLTEXT INDEX idx_slug_full_text_search (slug);
        `);
    },
  }
);

export default Bus;
