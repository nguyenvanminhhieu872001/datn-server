// models/TicketModel.js
import { Sequelize } from "sequelize";
import db from "../config/database.js";
import Booking from "./BookingModel.js";
import User from "./UserModel.js";

const { DataTypes } = Sequelize;

const Ticket = db.define(
  "Ticket",
  {
    type: {
      type: DataTypes.ENUM("unpaid", "paid", "canceled", "refunded"),
      allowNull: false,
      defaultValue: "unpaid",
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        isFloat: true,
        min: 0,
      },
    },
  },
  {
    freezeTableName: true,
  }
);

Ticket.belongsTo(User, { foreignKey: "UserId" });

Ticket.belongsTo(Booking, { foreignKey: "BookingId" });

export default Ticket;
