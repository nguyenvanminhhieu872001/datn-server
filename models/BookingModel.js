// models/Booking.js
import { Sequelize } from "sequelize";
import db from "../config/database.js";
import User from "./UserModel.js";
import Bus from "./BusModel.js";
import Route from "./RouteModel.js";

const { DataTypes } = Sequelize;

const Booking = db.define(
  "Booking",
  {
    startDate: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        isDate: true,
      },
    },
    endDate: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        isDate: true,
      },
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        isFloat: true,
        min: 0,
      },
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.ENUM("active", "ongoing", "canceled", "completed"),
      allowNull: false,
      defaultValue: "active",
      validate: {
        notEmpty: true,
      },
    },
  },
  {
    freezeTableName: true,
  }
);

Booking.belongsTo(Bus, { targetKey: "id", foreignKey: "busId" });
Booking.belongsTo(User, { targetKey: "id", foreignKey: "userId" });
Booking.belongsTo(Route, { targetKey: "id", foreignKey: "routeId" });

export default Booking;
