// models/PaymentHistoryModel.js
import { Sequelize } from "sequelize";
import db from "../config/database.js";
import Ticket from "./TicketModel.js";
import User from "./UserModel.js";

const { DataTypes } = Sequelize;

const PaymentHistory = db.define(
  "PaymentHistory",
  {
    status: {
      type: DataTypes.ENUM("success", "failure", "refunded"), // Thêm trạng thái "refunded"
      allowNull: false,
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: true,
      validate: {
        isFloat: true,
        min: 0,
      },
    },
    refund_amount: {
      type: DataTypes.FLOAT,
      allowNull: true,
      validate: {
        isFloat: true,
        min: 0,
      },
    },
    quantily: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isInt: true,
        min: 1,
      },
    },
  },
  {
    freezeTableName: true,
  }
);

PaymentHistory.belongsTo(User, { foreignKey: "UserId" });

PaymentHistory.belongsTo(Ticket, { foreignKey: "TicketId" });

export default PaymentHistory;
