import { Sequelize } from "sequelize";
import db from "../config/database.js";
import slugify from "slugify";

const { DataTypes } = Sequelize;

const Users = db.define(
  "Users",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
      indexes: [{ fields: ["name"] }],
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
        isEmail: true,
        is: /^[a-z0-9]*@gmail\.com$/,
      },
      unique: true,
      indexes: [{ fields: ["email"] }],
    },
    birth: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    gender: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: true,
      },
    },
    role: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 1,
      validate: {
        notEmpty: true,
      },
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [10, 12],
      },
      indexes: [{ fields: ["phone"] }],
    },
    license: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    expride: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    idCard: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [3, 100],
      },
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    slug: {
      type: DataTypes.STRING,
      unique: true,
      indexes: [{ fields: ["slug"] }],
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    hooks: {
      beforeValidate: (user, options) => {
        // Tạo giá trị slug từ giá trị name trước khi validate
        user.slug = slugify(user.name, { lower: true });
      },
    },
    afterSync: async () => {
      // Áp dụng chỉ mục Full-Text Search cho cột name, email, và phone
      await db.query(`
          ALTER TABLE Users ADD FULLTEXT INDEX idx_name_full_text_search (name);
          ALTER TABLE Users ADD FULLTEXT INDEX idx_email_full_text_search (email);
          ALTER TABLE Users ADD FULLTEXT INDEX idx_phone_full_text_search (phone);
          ALTER TABLE Users ADD FULLTEXT INDEX idx_slug_full_text_search (slug);
        `);
    },
  }
);

export default Users;
