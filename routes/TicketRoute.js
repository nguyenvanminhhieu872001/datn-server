import express from "express";
import * as ticketController from "../controllers/TicketController.js";

const router = express.Router();

router.post("/", ticketController.createTicket);
router.get("/", ticketController.getAllTickets);
router.get("/:id", ticketController.getTicketById);
router.get("/user/:id", ticketController.getTicketsByUserId);
router.put("/:id", ticketController.updateTicket);
router.delete("/:id", ticketController.deleteTicket);

export default router;
