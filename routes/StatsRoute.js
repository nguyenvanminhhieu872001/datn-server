import express from "express";
import * as Stats from "../controllers/StatsController.js";

const paymentRouter = express.Router();

paymentRouter.get("/start", Stats.getStats);

export default paymentRouter;
