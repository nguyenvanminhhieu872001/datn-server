import express from "express";
import * as bookingController from "../controllers/BookingController.js";

const router = express.Router();

router.post("/", bookingController.createBooking);
router.get("/:id", bookingController.getBookingDetails);
router.get("/", bookingController.getAllBookings);
router.put("/:id", bookingController.updateBooking);
router.delete("/:id", bookingController.deleteBooking);

export default router;
