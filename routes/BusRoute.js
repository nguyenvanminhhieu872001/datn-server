import express from "express";
import {
  validateCreateBus,
  validateUpdateBus,
} from "../Validations/validationMiddleware.js";
import * as busController from "../controllers/BusController.js";

const router = express.Router();

router.post("/", validateCreateBus, busController.createBus);
router.get("/", busController.getAllBuses);
router.get("/:id", busController.getBusById);
router.put("/:id", validateUpdateBus, busController.updateBus);
router.delete("/:id", busController.deleteBus);

export default router;
