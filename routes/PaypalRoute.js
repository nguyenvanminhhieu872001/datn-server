// routes/paymentRoutes.js
import express from "express";
import * as Payment from "../controllers/PaymentController.js";

const paymentRouter = express.Router();

// Đường dẫn để thanh toán
paymentRouter.post("/", Payment.payWithPaypal);

paymentRouter.get("/:id", Payment.payHistory);
paymentRouter.get("/", Payment.payHistories);

// Đường dẫn để hoàn trả
// paymentRouter.post("/refund/:paymentHistoryId", Payment.refundWithPaypal);

export default paymentRouter;
