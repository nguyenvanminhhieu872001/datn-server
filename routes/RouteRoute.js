import express from 'express';
import * as busValidate from '../Validations/validationMiddleware.js';
import * as routeController from '../controllers/RouteController.js';

const router = express.Router();

router.post(
	'/',
	busValidate.validateCreateRoute,
	routeController.createRoute
);
router.get('/', routeController.getAllRoute);
router.get('/:id', routeController.getRouteById);
router.put(
	'/:id',
	busValidate.validateUpdateRoute,
	routeController.updateRoute
);
router.delete('/:id', routeController.deleteRoute);

export default router;
