import express from "express";

import {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  activeMail,
  forgotPassword,
  resetPassword,
  changePassword,
  getDriverWithBooking,
} from "../controllers/UserController.js";
import { verifyUser, adminOnly } from "../middleware/AuthUser.js";

const router = express.Router();

router.get("/", getUsers);
router.get("/:id", getUserById);
router.post("/", createUser);
router.patch("/:id", updateUser);
router.delete("/:id", adminOnly, deleteUser);
router.post("/active", activeMail);
router.post("/forgot-password", forgotPassword);
router.patch("/forgot-password/:hash", resetPassword);
router.patch("/change-password", verifyUser, changePassword);
router.get("/get-driver", getDriverWithBooking);

export default router;
