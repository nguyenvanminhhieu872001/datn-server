import { Op, Sequelize } from "sequelize";

// Hàm tìm kiếm chung
async function searchByQuery(model, params, searchColumns) {
  console.log(params);
  try {
    const result = await model.findAll({
      where: {
        [Op.or]: searchColumns.map((column) => {
          return {
            [column]: {
              [Op.like]: `%${params[column] || ""}%`,
            },
          };
        }),
      },
    });

    return result;
  } catch (error) {
    console.error(`Error searching ${model.name}:`, error);
    throw error;
  }
}

export default searchByQuery;
