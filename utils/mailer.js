//mailer.js

import nodeMailer from "nodemailer";
import mailConfig from "../config/mail.config.js";

async function sendMail(to, subject, htmlContent) {
  const transport = nodeMailer.createTransport({
    host: mailConfig.HOST,
    port: mailConfig.PORT,
    secure: false,
    auth: {
      user: mailConfig.USERNAME,
      pass: mailConfig.PASSWORD,
    },
  });

  const options = {
    from: mailConfig.FROM_ADDRESS,
    to: to,
    subject: subject,
    html: htmlContent,
  };
  console.log("Trans :", transport, "\nOptions :", options);
  return transport.sendMail(options);
}

export default sendMail;
