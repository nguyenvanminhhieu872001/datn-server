import express from "express";
import session from "express-session";
import cors from "cors";
import dotenv from "dotenv";
import UserRoute from "./routes/UserRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
import BusRoute from "./routes/BusRoute.js";
import RouteRoute from "./routes/RouteRoute.js";
import BookingRoute from "./routes/BookingRoute.js";
import TicketRoute from "./routes/TicketRoute.js";
import PaypalRoute from "./routes/PaypalRoute.js";

import SequelizeStore from "connect-session-sequelize";
import db from "./config/database.js";
dotenv.config();

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
  db: db,
});

(async () => {
  await db.sync();
  store.sync();
})();

app.use(
  session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
      secure: "auto",
    },
  })
);
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:5173",
  })
);

app.use(AuthRoute);
app.use("/users", UserRoute);
app.use("/bus", BusRoute);
app.use("/route", RouteRoute);
app.use("/booking", BookingRoute);
app.use("/ticket", TicketRoute);
app.use("/pay", PaypalRoute);

app.listen(process.env.APP_PORT, () => {
  console.log("Server Up");
});
