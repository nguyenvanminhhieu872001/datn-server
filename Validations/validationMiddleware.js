import { body, param, validationResult } from "express-validator";

export const validateCreateBus = [
  body("name").isLength({ min: 3, max: 100 }).notEmpty(),
  body("seat").isNumeric().notEmpty(),
  body("type").isIn(["normal", "sleeper"]).notEmpty(),
  body("status")
    .isIn(["inactive", "active", "repair", "maintenance"])
    .optional(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

export const validateUpdateBus = [
  param("id").isInt().withMessage("Bus ID must be an integer"),
  body("name")
    .optional()
    .isLength({ min: 3, max: 100 })
    .withMessage("Name must be between 3 and 100 characters"),
  body("seat").optional().isNumeric().withMessage("Seat must be a number"),
  body("type")
    .optional()
    .isIn(["normal", "sleeper"])
    .withMessage("Invalid bus type"),
  body("status")
    .optional()
    .isIn(["inactive", "active", "repair", "maintenance"])
    .withMessage("Invalid bus status"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

export const validateCreateRoute = [
  body("startPoint").isLength({ min: 3, max: 100 }).notEmpty(),
  body("endPoint").isLength({ min: 3, max: 100 }).notEmpty(),
  body("frequency").isNumeric().notEmpty(),
  body("busTotal").isNumeric().notEmpty(),
  body("status")
    .isIn(["inactive", "active", "repair", "maintenance"])
    .notEmpty(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

export const validateUpdateRoute = [
  param("id").isInt().withMessage("Route ID must be an integer"),
  body("startPoint")
    .optional()
    .isLength({ min: 3, max: 100 })
    .withMessage("Start Point must be between 3 and 100 characters"),
  body("endPoint")
    .optional()
    .isLength({ min: 3, max: 100 })
    .withMessage("End Point must be between 3 and 100 characters"),
  body("frequency")
    .optional()
    .isNumeric()
    .withMessage("Frequency must be a number"),
  body("busTotal")
    .optional()
    .isNumeric()
    .withMessage("Bus Total must be a number"),
  body("status")
    .optional()
    .isIn(["inactive", "active", "repair", "maintenance"])
    .withMessage("Invalid Route status"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];
