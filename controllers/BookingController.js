import Booking from "../models/BookingModel.js";
import Bus from "../models/BusModel.js";
import User from "../models/UserModel.js";
import Route from "../models/RouteModel.js";
import Ticket from "../models/TicketModel.js";

export const createBooking = async (req, res) => {
  try {
    const { startDate, endDate, price, status, busId, routeId, userId } =
      req.body;
    // Kiểm tra xem busId, routeId, userId có tồn tại không
    const bus = await Bus.findByPk(busId);
    const route = await Route.findByPk(routeId);
    const user = await User.findByPk(userId);

    if (!bus || !route || !user) {
      return res.status(404).json({ message: "Bus, Route, or User not found" });
    }

    const currentBookings = await Booking.count({
      where: { routeId: routeId, status: "active" },
    });

    if (currentBookings >= route.frequency) {
      return res
        .status(400)
        .json({ message: "Booking limit reached for this route" });
    }

    // Kiểm tra xem userId đã có booking hoạt động chưa
    const existingUserBooking = await Booking.findOne({
      where: { userId: userId, status: "active" },
    });

    if (existingUserBooking) {
      return res
        .status(400)
        .json({ message: "User already has an active booking" });
    }

    // Kiểm tra xem busId đã có booking hoạt động chưa
    const existingBusBooking = await Booking.findOne({
      where: { busId: busId, status: "active" },
    });

    if (existingBusBooking) {
      return res
        .status(400)
        .json({ message: "Bus already has an active booking" });
    }

    // Tạo booking và liên kết với Buses, Bus, và User
    const booking = await Booking.create({
      startDate,
      endDate,
      price,
      status,
      busId: busId,
      routeId: routeId,
      userId: userId,
    });

    res.status(201).json(booking);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export const getBookingDetails = async (req, res) => {
  try {
    const bookingId = req.params.id;
    const booking = await Booking.findByPk(bookingId, {
      include: [
        {
          model: Route,
        },
        {
          model: Bus,
        },
        {
          model: User,
        },
      ],
    });

    if (!booking) {
      return res.status(404).json({ message: "Booking not found" });
    }

    const totalAmount = await Ticket.sum("amount", {
      where: { bookingId: booking.id, type: "paid" },
    });
    if (totalAmount == null) {
      totalAmount = 0;
    }

    const formattedBooking = {
      id: booking.id,
      startDate: booking.startDate,
      endDate: booking.endDate,
      createdAt: booking.createdAt,
      price: booking.price,
      status: booking.status,
      totalTicket: totalAmount,
      routeDetail: {
        id: booking.Route.id,
        startPoint: booking.Route.startPoint,
        endPoint: booking.Route.endPoint,
        frequency: booking.Route.frequency,
        busTotal: booking.Route.busTotal,
        status: booking.Route.status,
      },
      busDetail: {
        id: booking.Bus.id,
        name: booking.Bus.name,
        status: booking.Bus.status,
        type: booking.Bus.type,
        seat: booking.Bus.seat,
        amount: booking.Bus.amount,
      },
      userDetail: {
        id: booking.User.id,
        name: booking.User.name,
        email: booking.User.email,
        status: booking.User.status,
        role: booking.User.role,
        license: booking.User.license,
        expride: booking.User.expride,
        idCard: booking.User.idCard,
        phone: booking.User.phone,
      },
    };

    res.status(200).json(formattedBooking);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error", error });
  }
};

export const updateBooking = async (req, res) => {
  try {
    const bookingId = req.params.id;
    const updatedFields = req.body;

    // Check if the booking exists
    const booking = await Booking.findByPk(bookingId);
    if (!booking) {
      return res.status(404).json({ message: "Booking not found" });
    }
    // Update only the fields that are provided in the request body
    await booking.update(updatedFields);

    res.status(200).json({ message: "Booking updated successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error", error });
  }
};

export const deleteBooking = async (req, res) => {
  try {
    const bookingId = req.params.id;

    const booking = await Booking.findByPk(bookingId);
    if (!booking) {
      return res.status(404).json({ message: "Booking not found" });
    }
    const bookingTicket = await Ticket.findOne({
      where: {
        BookingId: req.params.id,
      },
    });
    if (bookingTicket) {
      try {
        await booking.update({
          status: "Disable",
        });
        return res.status(201).json({ msg: "Update to Disable Booking" });
      } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: error.message });
      }
    } else {
      await booking.destroy();

      res.status(204).json({ message: "Booking deleted successfully" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error", error });
  }
};

export const getAllBookings = async (req, res) => {
  try {
    const bookings = await Booking.findAll({
      include: [
        {
          model: Route,
        },
        {
          model: Bus,
        },
        {
          model: User,
        },
      ],
    });

    const formattedBookings = await Promise.all(
      bookings.map(async (booking) => {
        const totalAmount = await Ticket.sum("amount", {
          where: { bookingId: booking.id, type: "paid" },
        });
        if (totalAmount == null) {
          totalAmount = 0;
        }
        return {
          id: booking.id,
          startDate: booking.startDate,
          endDate: booking.endDate,
          createdAt: booking.createdAt,
          price: booking.price,
          status: booking.status,
          totalTicket: totalAmount,
          routeDetail: {
            id: booking.Route.id,
            startPoint: booking.Route.startPoint,
            endPoint: booking.Route.endPoint,
            frequency: booking.Route.frequency,
            busTotal: booking.Route.busTotal,
            status: booking.Route.status,
          },
          busDetail: {
            id: booking.Bus.id,
            name: booking.Bus.name,
            status: booking.Bus.status,
            type: booking.Bus.type,
            seat: booking.Bus.seat,
            amount: booking.Bus.amount,
          },
          userDetail: {
            id: booking.User.id,
            name: booking.User.name,
            email: booking.User.email,
            status: booking.User.status,
            role: booking.User.role,
            license: booking.User.license,
            expride: booking.User.expride,
            idCard: booking.User.idCard,
            phone: booking.User.phone,
          },
        };
      })
    );

    res.status(200).json(formattedBookings);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error", error });
  }
};
