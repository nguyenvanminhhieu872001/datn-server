import User from "../models/UserModel.js";
import searchByQuery from "../utils/search.js";
import argon2 from "argon2";
import mailer from "../utils/mailer.js";
import Booking from "../models/BookingModel.js";
import Ticket from "../models/TicketModel.js";
import Payment from "../models/PaymentHistoryModel.js";

export const getUsers = async (req, res) => {
  try {
    const response = await User.findAll();
    // const checkUserOnBus = await Booking.findAll({
    //   where: { userId: response.id, status: "active" },
    // });
    const formatResponse = response.map((user) => ({
      id: user.id,
      name: user.name,
      email: user.email,
      address: user.address,
      birth: user.birth,
      gender: user.gender,
      phone: user.phone,
      license: user.license,
      expride: user.expride,
      idCard: user.idCard,
      role: user.role,
      status: user.status,
      createdAt: user.createdAt,
      // userOnBus: checkUserOnBus?.length,
    }));
    res.status(200).json(formatResponse);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const getUserById = async (req, res) => {
  try {
    const response = await User.findOne({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const updateUser = async (req, res) => {
  const change = {
    name: req.body.name,
    email: req.body.email,
    address: req.body.address,
    birth: req.body.birth,
    gender: req.body.gender,
    phone: req.body.phone,
    license: req.body.license,
    expride: req.body.expride,
    idCard: req.body.idCard,
    password: req.body.password,
    rePassword: req.body.rePassword,
    hash: "",
  };

  const user = await User.findOne({
    where: {
      id: req.params.id,
    },
  });

  if (!user) {
    return res.status(404).json({ msg: "User Not Found" });
  }

  if (!change.password) {
    // If password is empty or null, keep the existing password
    change.password = user.password;
  } else if (change.password === change.rePassword) {
    // If passwords match, hash the new password
    change.password = await argon2.hash(change.password);
  } else {
    return res.status(400).json({ msg: "Password does not match" });
  }

  try {
    // Use update method directly on the user instance
    await user.update(change);
    res.status(200).json({ msg: "User updated successfully" });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

export const deleteUser = async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.params.id,
    },
  });
  if (!user) {
    return res.status(404).json({ msg: "User Not Found" });
  } else {
    const checkBooking = await Booking.findOne({
      where: {
        UserId: req.params.id,
      },
    });
    const checkTicket = await Ticket.findOne({
      where: {
        UserId: req.params.id,
      },
    });
    const checkPayment = await Payment.findOne({
      where: {
        UserId: req.params.id,
      },
    });

    if (checkBooking || checkTicket || checkPayment) {
      try {
        await user.update({
          status: 2,
        });
        return res.status(201).json({ msg: "Update to Disable User" });
      } catch (error) {
        return res.status(500).json({ msg: error.message });
      }
    } else {
      try {
        await user.destroy({
          where: {
            id: req.params.id,
          },
        });
        return res.status(201).json({ msg: "Delete Confirm" });
      } catch (error) {
        return res.status(500).json({ msg: error.message });
      }
    }
  }
};

export const createUser = async (req, res) => {
  const user = {
    name: req.body.name,
    email: req.body.email,
    address: req.body.address,
    birth: req.body.birth,
    gender: req.body.gender,
    phone: req.body.phone,
    license: req.body.license,
    expride: req.body.expride,
    idCard: req.body.idCard,
    password: req.body.password,
    rePassword: req.body.rePassword,
    role: req.body.role,
    status: 0,
    hash: "",
  };
  if (user.password != user.rePassword)
    return res.status(400).json({ msg: "Wrong Password Confirm" });
  try {
    // Sử dụng await để lấy giá trị hash
    const hashPassword = await argon2.hash(user.password);
    const hashEmail = await argon2.hash(user.email);
    user.password = hashPassword;
    user.hash = hashEmail;

    // Sử dụng await để tạo người dùng và lấy kết quả
    const createdUser = await User.create(user);

    const htmlContent = `<h1>Chào mừng bạn đến với Long Travel Ticket</h1>
            <p>Đây là email xác nhận tài khoản của bạn</p>
            <form action="http://localhost:8090/users/active" method="POST">
              <input type="hidden" name="email" value="${createdUser.email}" />
              <input type="hidden" name="hash" value="${hashEmail}" />
              <button type="submit">Click vào đây để xác nhận</button>
            </form>`;
    // Gửi email
    await mailer(createdUser.email, "Xác nhận tài khoản", htmlContent);

    res.status(201).json({ msg: "Register Confirm" });
  } catch (error) {
    res.status(500).json({ msg: "Sai form", error: error.message });
  }
};

export const activeMail = async (req, res) => {
  try {
    const data = {
      email: req.body.email,
      hash: req.body.hash.trim(),
    };

    const user = await User.findOne({
      where: {
        email: data.email,
      },
    });

    if (!user) {
      return res.status(404).json({ msg: "User Not Found" });
    }

    if (user.hash === data.hash) {
      await user.update(
        { status: 1, hash: "" },
        { where: { email: data.email } }
      );

      return res.redirect("http://localhost:5173/login");
    } else {
      return res.status(400).json({ msg: "Active Failed" });
    }
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

export const forgotPassword = async (req, res, next) => {
  try {
    const data = {
      email: req.body.email,
    };
    const user = await User.findOne({
      where: {
        email: data.email,
      },
    });
    if (!user) {
      return res.status(404).json({ msg: "User Not Found" });
    }
    const stringRandom = Math.random().toString(36).substring(2, 15);
    const hashes = await argon2.hash(stringRandom);
    const htmlContent = `<h1>Chào mừng bạn đến với Long Travel Ticket</h1>
            <p>Đây là email cấp lại mật khẩu tài khoản của bạn</p>
            <a href="http://localhost:8090/users/forgot-password/${hashes}" method="PATCH">Nhấn vào đây để thiết lập lại mật khẩu của bạn.</a>`;
    // Gửi email
    await mailer(user.email, "Yêu cầu cấp lại mật khẩu tài khoản", htmlContent);
    await user.update(
      { status: 1, hash: hashes },
      { where: { email: data.email } }
    );
    return res.status(201).json({ msg: "Recovery Password Send" });
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

export const resetPassword = async (req, res) => {
  try {
    const token = argon2.hash("123456").update(req.params.hash).digest("hex");
    const user = await User.findOne({
      where: {
        hash: token,
      },
    });
    if (!user) {
      return res.status(404).json({ msg: "User Not Found" });
    } else {
      if (req.body.password === req.body.rePassword) {
        const hashPassword = await argon2.hash(req.body.password);
        await user.update(
          { password: hashPassword, hash: "" },
          { where: { hash: token } }
        );
        return res.status(201).json({ msg: "Reset Password Confirm" });
      }
    }
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

export const changePassword = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!user) {
      return res.status(404).json({ msg: "User Not Found" });
    } else {
      if (req.body.password === req.body.rePassword) {
        const hashPassword = await argon2.hash(req.body.password);
        await user.update(
          { password: hashPassword },
          { where: { id: req.params.id } }
        );
        return res.status(201).json({ msg: "Change Password Confirm" });
      }
    }
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

export const getDriverWithBooking = async (req, res) => {
  try {
    // Get drivers with role 2 (Driver)
    const drivers = await User.findAll({
      where: {
        role: 2,
      },
    });

    // Get active bookings
    const activeBookings = await Booking.findAll({
      where: {
        status: "active",
      },
    });

    // Separate drivers into two arrays based on whether they have an active booking or not
    const driversWithActiveBooking = [];
    const driversWithoutActiveBooking = [];

    drivers.forEach((driver) => {
      const hasActiveBooking = activeBookings.some(
        (booking) => booking.userId === driver.id
      );

      if (hasActiveBooking) {
        driversWithActiveBooking.push(driver);
      } else {
        driversWithoutActiveBooking.push(driver);
      }
    });

    const formattedDriversWithActiveBooking = driversWithActiveBooking.map(
      async (driversWithActiveBooking) => {
        return {
          id: driversWithActiveBooking.id,
          name: driversWithActiveBooking.name,
          email: driversWithActiveBooking.email,
          address: driversWithActiveBooking.address,
          birth: driversWithActiveBooking.birth,
          gender: driversWithActiveBooking.gender,
          phone: driversWithActiveBooking.phone,
          license: driversWithActiveBooking.license,
          expride: driversWithActiveBooking.expride,
          idCard: driversWithActiveBooking.idCard,
          role: driversWithActiveBooking.role,
          status: driversWithActiveBooking.status,
          createdAt: driversWithActiveBooking.createdAt,
        };
      }
    );

    res.status(200).json({
      formattedDriversWithActiveBooking,
    });
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
