// controllers/StatsController.js
import Ticket from "../models/TicketModel.js";
import PaymentHistory from "../models/PaymentHistoryModel.js";
import { Op } from "sequelize";

export const getStats = async (req, res) => {
  try {
    const { startDate, endDate, groupBy } = req.query;

    // Thiết lập điều kiện thời gian nếu có
    const dateCondition = {};
    if (startDate && endDate) {
      dateCondition.createdAt = {
        [Op.between]: [new Date(startDate), new Date(endDate)],
      };
    }

    // Lựa chọn thuộc tính để nhóm theo
    let groupAttribute;
    switch (groupBy) {
      case "route":
        groupAttribute = "routeId";
        break;
      case "bus":
        groupAttribute = "busId";
        break;
      case "booking":
        groupAttribute = "BookingId";
        break;
      default:
        groupAttribute = null;
        break;
    }

    if (!groupAttribute) {
      return res.status(400).json({ error: "Invalid groupBy parameter" });
    }

    // Thống kê tổng số vé đặt, số tiền thu vào và số tiền hoàn trả theo groupAttribute
    const stats = await Ticket.findAll({
      attributes: [
        groupAttribute,
        [
          Ticket.sequelize.fn("COUNT", Ticket.sequelize.col("id")),
          "totalBookings",
        ],
        [
          Ticket.sequelize.fn("SUM", Ticket.sequelize.col("price")),
          "totalRevenue",
        ],
        [
          Ticket.sequelize.fn(
            "SUM",
            Ticket.sequelize.literal(
              '(CASE WHEN "type" = \'refunded\' THEN "price" ELSE 0 END)'
            )
          ),
          "totalRefund",
        ],
      ],
      where: dateCondition,
      include: {
        model: PaymentHistory,
        attributes: [],
        required: false,
      },
      group: [groupAttribute],
    });

    // Chuyển đổi dữ liệu thành đối tượng để trả về
    const result = stats.map((stat) => ({
      [groupAttribute]: stat[groupAttribute],
      totalBookings: stat.get("totalBookings"),
      totalRevenue: stat.get("totalRevenue"),
      totalRefund: stat.get("totalRefund"),
    }));

    res.json({ stats: result });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
