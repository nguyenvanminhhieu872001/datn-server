// controllers/PaymentController.js
import Ticket from "../models/TicketModel.js";
import PaymentHistory from "../models/PaymentHistoryModel.js";
import Users from "../models/UserModel.js";
import Booking from "../models/BookingModel.js";
import Bus from "../models/BusModel.js";
import Redis from "redis";
import { promisify } from "util";

// const redisClient = Redis.createClient();
// const getAsync = promisify(redisClient.get).bind(redisClient);
// const setAsync = promisify(redisClient.set).bind(redisClient);
// const delAsync = promisify(redisClient.del).bind(redisClient);

export const payWithPaypal = async (req, res) => {
  const { ticketId, userId, quantily } = req.body;
  // const lockKey = `ticket_lock:${ticketId}`;

  try {
    // Acquire lock
    // const lock = await setAsync(lockKey, "locked", "NX", "EX", 10); // Lock expires in 10 seconds

    // if (!lock) {
    //   return res.status(409).json({
    //     error: "Ticket is being processed by another user. Please try again.",
    //   });
    // }

    const ticket = await Ticket.findByPk(ticketId, {
      attributes: ["id", "type", "amount", "price", "BookingId"],
    });

    if (!ticket) {
      return res.status(404).json({ error: "Ticket not found" });
    }

    const booking = await Booking.findByPk(ticket.BookingId, {
      include: [
        {
          model: Bus,
        },
      ],
    });

    if (booking.Bus.seat < booking.amount + quantily) {
      return res.status(400).json({ error: "Not enough seat" });
    } else {
      PaymentHistory.create({
        status: "success",
        amount: ticket.price * quantily,
        quantily: quantily,
        TicketId: ticketId,
        UserId: userId,
      });

      ticket.update({ type: "paid", amount: quantily });

      if (booking.amount === booking.Bus.seat + quantily) {
        booking.update({ status: "full" });
      }

      console.log("booking\n", booking.amount);
      console.log("quantily\n", quantily);
      booking.update({ amount: booking.amount + quantily });

      // Release lock
      // await delAsync(lockKey);

      res.status(200).json({ message: "Payment success" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

export const payHistory = async (req, res) => {
  const { userId } = req.params;
  try {
    const history = await PaymentHistory.findAll({
      where: { UserId: userId },
      include: [
        { model: Ticket, as: "ticket" },
        {
          model: Users,
          as: "user",
          attributes: ["id", "name", "email", "phone", "address"],
        },
      ],
    });
    res.status(200).json({ history });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

export const payHistories = async (req, res) => {
  try {
    const histories = await PaymentHistory.findAll({
      include: [
        { model: Ticket, as: "ticket" },
        {
          model: Users,
          as: "user",
          attributes: ["id", "name", "email", "phone", "address"],
        },
      ],
    });
    res.status(200).json({ histories });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
