import Route from "../models/RouteModel.js";
import Booking from "../models/BookingModel.js";

export const createRoute = async (req, res) => {
  try {
    const { startPoint, endPoint, frequency, busTotal, status } = req.body;
    const newRoute = await Route.create({
      startPoint,
      endPoint,
      frequency,
      busTotal,
      status,
    });
    res.status(201).json(newRoute);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export const getAllRoute = async (req, res) => {
  try {
    const newRoute = await Route.findAll();
    res.status(200).json(newRoute);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export const getRouteById = async (req, res) => {
  const busId = req.params.id;

  try {
    const Routes = await Route.findByPk(busId);
    if (!Routes) {
      return res.status(404).json({ message: "Route not found" });
    }
    res.status(200).json(Routes);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export const updateRoute = async (req, res) => {
  const RouteId = req.params.id;

  try {
    const existingRoute = await Route.findByPk(RouteId);
    if (!existingRoute) {
      return res.status(404).json({ message: "Route not found" });
    }

    const { startPoint, endPoint, frequency, busTotal, status } = req.body;

    if (startPoint) {
      existingRoute.startPoint = startPoint;
    }
    if (endPoint) {
      existingRoute.endPoint = endPoint;
    }
    if (frequency) {
      existingRoute.frequency = frequency;
    }
    if (busTotal) {
      existingRoute.busTotal = busTotal;
    }
    if (status) {
      existingRoute.status = status;
    }

    await existingRoute.save();

    res.status(200).json(existingRoute);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export const deleteRoute = async (req, res) => {
  const RouteId = req.params.id;

  try {
    const Routes = await Route.findByPk(RouteId);
    if (!Routes) {
      return res.status(404).json({ message: "Route not found" });
    }

    const associatedBookings = await Booking.findOne({
      where: {
        RouteId: RouteId,
      },
    });
    if (associatedBookings) {
      try {
        await Routes.update({
          status: "disable",
        });
        return res.status(201).json({ msg: "Update to Disable Route" });
      } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: error.message });
      }
    }
    await Routes.destroy();

    return res.status(204).json({ message: "Route Deleted" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};
