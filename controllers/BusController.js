// busController.js
import Bus from "../models/BusModel.js";
import Booking from "../models/BookingModel.js";

export const createBus = async (req, res) => {
  try {
    const { name, seat, type, status } = req.body;
    const newBus = await Bus.create({ name, seat, type, status });
    return res.status(201).json(newBus);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

export const getAllBuses = async (req, res) => {
  try {
    const buses = await Bus.findAll();
    return res.status(200).json(buses);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

export const getBusById = async (req, res) => {
  const busId = req.params.id;

  try {
    const bus = await Bus.findByPk(busId);
    if (!bus) {
      return res.status(404).json({ message: "Bus not found" });
    }
    res.status(200).json(bus);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};
export const updateBus = async (req, res) => {
  const busId = req.params.id;

  try {
    // Lấy thông tin bus hiện tại từ database
    const existingBus = await Bus.findByPk(busId);

    // Kiểm tra xem bus có tồn tại không
    if (!existingBus) {
      return res.status(404).json({ message: "Bus not found" });
    }

    // Lấy thông tin mới từ request body
    const { name, seat, type, status } = req.body;

    // Cập nhật thông tin bus chỉ khi có dữ liệu mới từ form
    if (name) {
      existingBus.name = name;
    }
    if (seat) {
      existingBus.seat = seat;
    }
    if (type) {
      existingBus.type = type;
    }
    if (status) {
      existingBus.status = status;
    }

    // Lưu thông tin bus đã cập nhật vào database
    await existingBus.save();

    // Trả về thông tin bus sau khi cập nhật
    return res.status(200).json(existingBus);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

export const deleteBus = async (req, res) => {
  const busId = req.params.id;

  try {
    const existingBus = await Bus.findByPk(busId);
    if (!existingBus) {
      return res.status(404).json({ message: "Bus not found" });
    }

    const associatedBookings = await Booking.findOne({
      where: {
        BusId: busId,
      },
    });

    console.log("associatedBookings", associatedBookings);
    if (associatedBookings) {
      try {
        await existingBus.update({
          status: "disable",
        });
        return res.status(201).json({ msg: "Update to Disable Bus" });
      } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: error.message });
      }
    }
    await existingBus.destroy();
    return res.status(200).json({ message: "Bus deleted successfully" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};
