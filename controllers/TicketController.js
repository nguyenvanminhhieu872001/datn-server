import Ticket from "../models/TicketModel.js";
import Booking from "../models/BookingModel.js";
import Bus from "../models/BusModel.js";
import User from "../models/UserModel.js";
import Route from "../models/RouteModel.js";
import Payment from "../models/PaymentHistoryModel.js";

// Tạo Ticket mới
export const createTicket = async (req, res) => {
  try {
    const { bookingId, buyer, amount } = req.body;

    // Kiểm tra xem bookingId có tồn tại không
    const booking = await Booking.findByPk(bookingId, {
      include: [
        {
          model: Bus,
        },
      ],
    });

    if (!booking) {
      return res.status(404).json({ message: "Booking not found" });
    }

    if (booking.Bus.seat < booking.amount + amount) {
      return res.status(404).json({ message: "Not enough seat left" });
    }

    if (booking.status !== "active") {
      return res.status(404).json({ message: "Booking is not active" });
    }

    const buyerInfo = await User.findByPk(buyer);

    if (!buyerInfo) {
      return res.status(404).json({ message: "Buyer not found" });
    }

    const ticket = await Ticket.create({
      price: booking.price,
      amount: amount,
      BookingId: bookingId,
      UserId: buyer,
    });
    res.status(201).json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Cập nhật thông tin Ticket
export const updateTicket = async (req, res) => {
  try {
    const { amount } = req.body;
    const ticketId = req.params.id;

    // Kiểm tra xem ticketId có tồn tại không
    const ticket = await Ticket.findByPk(ticketId);

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    const booking = await Booking.findByPk(ticket.BookingId, {
      include: [
        {
          model: Bus,
        },
      ],
    });

    if (!booking) {
      return res.status(404).json({ message: "Booking not found" });
    }
    // const totalSeat = parseInt(amount);
    if (booking.Bus.seat < booking.amount + amount) {
      return res.status(404).json({ message: "Not enough seat left" });
    }

    // Cập nhật thông tin Ticket
    await ticket.update({ amount });

    res.status(200).json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Xóa Ticket
export const deleteTicket = async (req, res) => {
  try {
    const ticketId = req.params.id;

    const ticket = await Ticket.findByPk(ticketId);

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    const associatedPayment = await Payment.findOne({
      where: {
        TicketId: ticketId,
      },
    });

    if (associatedPayment) {
      await ticket.update({
        type: "Disable",
      });
    } else {
      await ticket.destroy();
    }

    return res.status(204).json({ message: "Ticket updated to Disable" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

// Lấy thông tin một Ticket theo id
export const getTicketById = async (req, res) => {
  try {
    const ticketId = req.params.id;

    // Kiểm tra xem ticketId có tồn tại không
    const ticket = await Ticket.findByPk(ticketId, {
      include: [
        {
          model: Booking,
          include: [
            {
              model: Route,
            },
            {
              model: Bus,
            },
            {
              model: User,
            },
          ],
        },
        {
          model: User, // Thêm thông tin người mua
          as: "User",
        },
      ],
    });

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    const totalAmount = await Ticket.sum("amount", {
      where: { bookingId: ticket.BookingId, type: "paid" },
    });
    if (totalAmount == null) {
      totalAmount = 0;
    }
    const busSeat = ticket.Booking.Bus.seat;

    const remainTicket = busSeat - totalAmount;

    const formattedTicket = {
      id: ticket.id,
      type: ticket.type,
      price: ticket.price,
      startDate: ticket.Booking.startDate,
      endDate: ticket.Booking.endDate,
      amount: ticket.amount,
      remainTicket: remainTicket,
      BookingId: ticket.BookingId,
      booking: {
        Route: {
          id: ticket.Booking.Route.id,
          startPoint: ticket.Booking.Route.startPoint,
          endPoint: ticket.Booking.Route.endPoint,
          frequency: ticket.Booking.Route.frequency,
          busTotal: ticket.Booking.Route.busTotal,
          status: ticket.Booking.Route.status,
        },
        Bus: {
          id: ticket.Booking.Bus.id,
          name: ticket.Booking.Bus.name,
          seat: ticket.Booking.Bus.seat,
          type: ticket.Booking.Bus.type,
          status: ticket.Booking.Bus.status,
          amount: ticket.Booking.Bus.amount,
        },
        Driver: {
          id: ticket.Booking.User.id,
          name: ticket.Booking.User.name,
          email: ticket.Booking.User.email,
          status: ticket.Booking.User.status,
          role: ticket.Booking.User.role,
        },
      },
      buyer: {
        // Thêm thông tin người mua vào ticket
        id: ticket.User.id,
        name: ticket.User.name,
        email: ticket.User.email,
        status: ticket.User.status,
        role: ticket.User.role,
      },
    };

    res.status(200).json(formattedTicket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Lấy tất cả Ticket
export const getAllTickets = async (req, res) => {
  try {
    const tickets = await Ticket.findAll({
      include: [
        {
          model: Booking,
          include: [
            {
              model: Route,
            },
            {
              model: Bus,
            },
            {
              model: User,
            },
          ],
        },
        {
          model: User, // Thêm thông tin người mua
          as: "User",
        },
      ],
    });
    if (tickets.length === 0) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    const formattedTickets = await Promise.all(
      tickets.map(async (ticket) => {
        const totalAmount = await Ticket.sum("amount", {
          where: { bookingId: ticket.BookingId, type: "paid" },
        });
        if (totalAmount == null) {
          totalAmount = 0;
        }
        const busSeat = ticket.Booking.Bus.seat;

        const remainTicket = busSeat - totalAmount;

        return {
          id: ticket.id,
          type: ticket.type,
          price: ticket.price,
          startDate: ticket.Booking.startDate,
          endDate: ticket.Booking.endDate,
          amount: ticket.amount,
          remainTicket: remainTicket,
          BookingId: ticket.BookingId,
          booking: {
            Route: {
              id: ticket.Booking.Route.id,
              startPoint: ticket.Booking.Route.startPoint,
              endPoint: ticket.Booking.Route.endPoint,
              frequency: ticket.Booking.Route.frequency,
              busTotal: ticket.Booking.Route.busTotal,
              status: ticket.Booking.Route.status,
            },
            Bus: {
              id: ticket.Booking.Bus.id,
              name: ticket.Booking.Bus.name,
              seat: ticket.Booking.Bus.seat,
              type: ticket.Booking.Bus.type,
              status: ticket.Booking.Bus.status,
              amount: ticket.Booking.Bus.amount,
            },
            Driver: {
              id: ticket.Booking.User.id,
              name: ticket.Booking.User.name,
              email: ticket.Booking.User.email,
              status: ticket.Booking.User.status,
              role: ticket.Booking.User.role,
            },
          },
          buyer: {
            // Thêm thông tin người mua vào ticket
            id: ticket.User.id,
            name: ticket.User.name,
            email: ticket.User.email,
            status: ticket.User.status,
            role: ticket.User.role,
          },
        };
      })
    );

    res.status(200).json(formattedTickets);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Lấy ticket theo User
export const getTicketsByUserId = async (req, res) => {
  try {
    const userId = req.params.userId;

    // Kiểm tra xem userId có tồn tại không
    const user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Lấy tất cả vé của người dùng
    const tickets = await Ticket.findAll({
      where: {
        UserId: userId,
      },
      include: [
        {
          model: Booking,
          include: [
            {
              model: Route,
            },
            {
              model: Bus,
            },
            {
              model: User, // Thêm thông tin người mua vé
              as: "User",
            },
          ],
          model: Payment,
        },
      ],
    });

    // Định dạng lại dữ liệu trước khi trả về
    const formattedTickets = await Promise.all(
      tickets.map(async (ticket) => {
        const totalAmount = await Ticket.sum("amount", {
          where: { bookingId: ticket.BookingId, type: "paid" },
        });
        if (totalAmount == null) {
          totalAmount = 0;
        }
        const busSeat = ticket.Booking.Bus.seat;

        const remainTicket = busSeat - totalAmount;

        return {
          id: ticket.id,
          type: ticket.type,
          price: ticket.price,
          startDate: ticket.Booking.startDate,
          endDate: ticket.Booking.endDate,
          amount: ticket.amount,
          remainTicket: remainTicket,
          BookingId: ticket.BookingId,
          booking: {
            Route: {
              id: ticket.Booking.Route.id,
              startPoint: ticket.Booking.Route.startPoint,
              endPoint: ticket.Booking.Route.endPoint,
              frequency: ticket.Booking.Route.frequency,
              busTotal: ticket.Booking.Route.busTotal,
              status: ticket.Booking.Route.status,
            },
            Bus: {
              id: ticket.Booking.Bus.id,
              name: ticket.Booking.Bus.name,
              seat: ticket.Booking.Bus.seat,
              type: ticket.Booking.Bus.type,
              status: ticket.Booking.Bus.status,
              amount: ticket.Booking.Bus.amount,
            },
            Driver: {
              id: ticket.Booking.User.id,
              name: ticket.Booking.User.name,
              email: ticket.Booking.User.email,
              status: ticket.Booking.User.status,
              role: ticket.Booking.User.role,
            },
          },
          buyer: {
            // Thêm thông tin người mua vào ticket
            id: ticket.User.id,
            name: ticket.User.name,
            email: ticket.User.email,
            status: ticket.User.status,
            role: ticket.User.role,
          },
        };
      })
    );

    res.status(200).json(formattedTickets);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};
