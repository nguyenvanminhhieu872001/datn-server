import User from "../models/UserModel.js";
import argon2d from "argon2";

export const Login = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (!user) return res.status(404).json({ msg: "User Not Found" });

    const match = await argon2d.verify(user.password, req.body.password);

    if (!match) return res.status(400).json({ msg: "Wrong Password" });

    req.session.userId = user.id;
    const resUser = {
      userId: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
    };

    res.status(200).json(resUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: "Internal Server Error" });
  }
};

export const logOut = async (req, res) => {
  try {
    req.session.destroy((err) => {
      if (err) {
        return res.status(400).json({ msg: "Cannot Logout.", error: err });
      }
      res.status(200).json({ msg: "Logout Success" });
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: "Internal Server Error" });
  }
};

export const Me = async (req, res) => {
  try {
    if (!req.session.userId) {
      return res.status(401).json({ msg: "Not login yet." });
    } else {
      const user = await User.findByPk(req.session.userId, {
        attributes: ["id", "name", "email", "role"],
      });

      if (!user) {
        return res.status(404).json({ msg: "User Not Found" });
      }

      res.status(200).json(user);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: "Internal Server Error" });
  }
};
