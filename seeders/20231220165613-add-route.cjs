"use strict";

/** @type {import('sequelize-cli').Migration} */
const faker = require("faker");
const slugify = require("slugify");

module.exports = {
  async up(queryInterface, Sequelize) {
    var dummyJSON = [];
    var pointJSON = [
      "Hà Nội",
      "Hà Giang",
      "Cao Bằng",
      "Bắc Kạn",
      "Tuyên Quang",
      "Lào Cai",
      "Điện Biên",
      "Lai Châu",
      "Sơn La",
      "Yên Bái",
      "Hoà Bình",
      "Thái Nguyên",
      "Lạng Sơn",
      "Quảng Ninh",
      "Bắc Giang",
      "Phú Thọ",
      "Vĩnh Phúc",
      "Bắc Ninh",
      "Hải Dương",
      "Hải Phòng",
      "Hưng Yên",
      "Thái Bình",
      "Hà Nam",
      "Nam Định",
      "Ninh Bình",
      "Thanh Hóa",
      "Nghệ An",
      "Hà Tĩnh",
      "Quảng Bình",
      "Quảng Trị",
      "Thừa Thiên Huế",
      "Đà Nẵng",
      "Quảng Nam",
      "Quảng Ngãi",
      "Bình Định",
      "Phú Yên",
      "Khánh Hòa",
      "Ninh Thuận",
      "Bình Thuận",
      "Kon Tum",
      "Gia Lai",
      "Đắk Lắk",
      "Đắk Nông",
      "Lâm Đồng",
      "Bình Phước",
      "Tây Ninh",
      "Bình Dương",
      "Đồng Nai",
      "Bà Rịa - Vũng Tàu",
      "Hồ Chí Minh",
      "Long An",
      "Tiền Giang",
      "Bến Tre",
      "Trà Vinh",
      "Vĩnh Long",
      "Đồng Tháp",
      "An Giang",
      "Kiên Giang",
      "Cần Thơ",
      "Hậu Giang",
      "Sóc Trăng",
      "Bạc Liêu",
      "Cà Mau",
    ];
    for (var i = 0; i < 15; i++) {
      const fakerStart = faker.random.arrayElement(pointJSON);
      const fakerEnd = faker.random.arrayElement(pointJSON);
      dummyJSON.push({
        startPoint: fakerStart,
        endPoint: fakerEnd,
        frequency: faker.datatype.number({ min: 1, max: 6 }),
        busTotal: faker.datatype.number({ min: 1, max: 6 }),
        status: faker.random.arrayElement([
          "inactive",
          "active",
        ]),
        slug: slugify(`${fakerStart}-${fakerEnd}`),
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }

    return queryInterface.bulkInsert("route", dummyJSON);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("route", {}, null);
  },
};
