// seeds/PaymentHistorySeed.js
"use strict";

/** @type {import('sequelize-cli').Migration} */
const faker = require("faker");

module.exports = {
  async up(queryInterface, Sequelize) {
    const users = await queryInterface.sequelize.query("SELECT id FROM users", {
      type: Sequelize.QueryTypes.SELECT,
    });

    const tickets = await queryInterface.sequelize.query(
      "SELECT id FROM ticket",
      {
        type: Sequelize.QueryTypes.SELECT,
      }
    );

    var dummyJSON = [];
    for (var i = 0; i < 10; i++) {
      const status = faker.random.arrayElement([
        "success",
        "failure",
      ]);
      const amount = faker.datatype.number({ min: 50, max: 200 });
      const refundAmount = faker.datatype.number({ min: 0, max: amount });
      const quantity = faker.datatype.number({ min: 0, max: 5 });
      const userId = faker.random.arrayElement(users).id;
      const ticketId = faker.random.arrayElement(tickets).id;

      dummyJSON.push({
        status,
        amount,
        refund_amount: refundAmount,
        quantily: quantity,
        UserId: userId,
        TicketId: ticketId,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }

    return queryInterface.bulkInsert("paymenthistory", dummyJSON);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("paymenthistory", {}, null);
  },
};
