"use strict";

/** @type {import('sequelize-cli').Migration} */
const faker = require("faker");
const slugify = require("slugify");

module.exports = {
  async up(queryInterface, Sequelize) {
    var dummyJSON = [];
    for (var i = 0; i < 20; i++) {
      const fakerName = faker.random.arrayElement([
        "Limousine 7",
        "Limousine 20",
        "Hyundai 45",
        "Thaco 16",
        "Thaco 47",
        "Fuso 29",
        "Gaz 12",
        "King Long 29",
      ]);
      const seat = fakerName.match(/\d+/)
        ? parseInt(fakerName.match(/\d+/)[0])
        : null;
      const type = seat && seat <= 20 ? "sleeper" : "normal";
      dummyJSON.push({
        name: fakerName,
        seat: seat,
        type: type,
        status: faker.random.arrayElement([
          "inactive",
          "active",
        ]),
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
    return queryInterface.bulkInsert("bus", dummyJSON);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete("bus", {}, null);
  },
};
