'use strict';

/** @type {import('sequelize-cli').Migration} */
const faker = require('faker');
const fakerVI = require('faker/locale/vi');
const slugify = require('slugify');

console.log(fakerVI);
module.exports = {
	async up(queryInterface, Sequelize) {
		var dummyJSON = [];
		for (var i = 0; i < 20; i++) {
			const fakerName = fakerVI.name.findName();
			dummyJSON.push({
				name: fakerName,
				address: fakerVI.address.city(),
				email: faker.internet
					.email({
						provider: 'gmail.com',
						allowSpecialCharacters: false,
					})
					.toLowerCase(),
				birth: faker.date.past(),
				gender: faker.datatype.number(1),
				password: faker.internet.password(),
				role: faker.datatype.number(1),
				phone: fakerVI.phone.phoneNumber(),
				license: faker.random.arrayElement(['B3', 'B2']),
				expride: faker.date.future(),
				slug: slugify(fakerName),
				idCard: faker.datatype.number({ min: 3, max: 12 }),
				status: faker.datatype.number(1),
				createdAt: new Date(),
				updatedAt: new Date(),
			});
		}
		/**
		 * Add seed commands here.
		 *
		 * Example:
		 * await queryInterface.bulkInsert('People', [{
		 *   name: 'John Doe',
		 *   isBetaMember: false
		 * }], {});
		 */

		return queryInterface.bulkInsert('users', dummyJSON);
	},

	async down(queryInterface, Sequelize) {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */

		return queryInterface.bulkDelete('users', {}, null);
	},
};
