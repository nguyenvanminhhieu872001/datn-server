"use strict";

/** @type {import('sequelize-cli').Migration} */
const faker = require("faker");

module.exports = {
  async up(queryInterface, Sequelize) {
    const users = await queryInterface.sequelize.query("SELECT id FROM users", {
      type: Sequelize.QueryTypes.SELECT,
    });

    const bookings = await queryInterface.sequelize.query(
      "SELECT id FROM booking",
      {
        type: Sequelize.QueryTypes.SELECT,
      }
    );

    var dummyJSON = [];
    for (var i = 0; i < 10; i++) {
      const type = faker.random.arrayElement([
        "unpaid",
        "paid",
      ]);
      const amount = faker.datatype.number(5);
      const price = faker.datatype.number({ min: 300, max: 600 });

      const userId = faker.random.arrayElement(users).id;
      const bookingId = faker.random.arrayElement(bookings).id;

      dummyJSON.push({
        type,
        amount,
        price,
        UserId: userId,
        BookingId: bookingId,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }

    return queryInterface.bulkInsert("ticket", dummyJSON);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("ticket", {}, null);
  },
};
