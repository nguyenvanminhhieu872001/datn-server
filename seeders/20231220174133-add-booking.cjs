"use strict";

/** @type {import('sequelize-cli').Migration} */
const faker = require("faker");
const fakerVI = require("faker/locale/vi");

module.exports = {
  async up(queryInterface, Sequelize) {
    const buses = await queryInterface.sequelize.query(
      "SELECT id FROM bus WHERE status = 'active'",
      {
        type: Sequelize.QueryTypes.SELECT,
      }
    );

    const routes = await queryInterface.sequelize.query(
      "SELECT id FROM route WHERE status = 'active'",
      {
        type: Sequelize.QueryTypes.SELECT,
      }
    );

    const users = await queryInterface.sequelize.query(
      "SELECT id FROM users WHERE status IN (0, 1) AND role = '1'",
      {
        type: Sequelize.QueryTypes.SELECT,
      }
    );

    var dummyJSON = [];
    for (var i = 0; i < 10; i++) {
      const startDate = fakerVI.date.future();
      const endDate = fakerVI.date.future();
      const price = faker.datatype.number({ min: 300, max: 600 });
      const amount = faker.datatype.number(5);
      const status = faker.random.arrayElement(["active"]);
      const busId = faker.random.arrayElement(buses).id;
      const routeId = faker.random.arrayElement(routes).id;
      const userId = faker.random.arrayElement(users).id;

      dummyJSON.push({
        startDate,
        endDate,
        price,
        amount,
        status,
        busId,
        userId,
        routeId,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }

    return queryInterface.bulkInsert("booking", dummyJSON);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete("booking", {}, null);
  },
};
