import User from "../models/UserModel.js";

export const verifyUser = async (req, res, next) => {
  try {
    if (!req.session.userId) {
      return res.status(401).json({ msg: "Not Login Yet!" });
    }
    const user = await User.findByPk(req.session.userId);
    if (!user) return res.status(404).json({ msg: "Can't Find" });

    req.user = user; // Gán user vào req để có thể sử dụng trong các middleware sau
    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: "Internal Server Error" });
  }
};

export const adminOnly = async (req, res, next) => {
  try {
    if (!req.user) {
      return res.status(401).json({ msg: "Not Login Yet!" });
    }

    if (req.user.role !== "0" && req.user.role !== "1") {
      return res.status(403).json({ msg: "Not Admin" });
    }
    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: "Internal Server Error" });
  }
};
